from geometry import lingkaran
from geometry import persegi
from geometry import kubus
import sys
import matplotlib.pyplot as plt

try:
    geo_circle = lingkaran.Lingkaran(10)
    geo_circle.hitung_luas()
    persegi.hitung_luas(10)
    kubus.hitung_volume(10)
    print("Berhasil melakukan import package")
except:
    print("Error saat import")